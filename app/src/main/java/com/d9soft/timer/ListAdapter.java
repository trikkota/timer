package com.d9soft.timer;

/**
 * Created by Grishanya on 24.01.2016.
 */

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


public class ListAdapter extends BaseAdapter {


    Context ctx;
    LayoutInflater lInflater;
    ArrayList<TimerObject> objects;
    Map<String, Timer> timerTasks;

    ListAdapter(Context context, ArrayList<TimerObject> timers) {
        ctx = context;
        objects = timers;
        lInflater = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        timerTasks = new HashMap<String, Timer>();
    }

    // elements count
    @Override
    public int getCount() {
        return objects.size();
    }

    // element on position
    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    // id on position
    @Override
    public long getItemId(int position) {
        return ((TimerObject)getItem(position)).getId();
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.item, parent, false);
        }

        TextView tvName = (TextView)view.findViewById(R.id.tvName);
        tvName.setText(objects.get(position).name);
        TextView tvTimeTotal = (TextView)view.findViewById(R.id.tvTimeTotal);
        tvTimeTotal.setText(MainActivity.millisToString(objects.get(position).timeTotal));
        TextView tvTimeLeft = (TextView)view.findViewById(R.id.tvTimeLeft);
        tvTimeLeft.setText(MainActivity.millisToString(objects.get(position).timeLeft));
        return view;
    }

    // timer on position
    TimerObject getTimer(int position) {
        return ((TimerObject) getItem(position));
    }

}
