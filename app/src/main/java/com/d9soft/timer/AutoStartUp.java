package com.d9soft.timer;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

/**
 * Created by Grishanya on 29.01.2016.
 */
public class AutoStartUp extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.e("TIMERS", "STARTUP");
        SQLiteDatabase DB = context.openOrCreateDatabase("timers.db", Context.MODE_PRIVATE, null);
        Cursor c = DB.rawQuery("SELECT * FROM Timers", null);
        if (c.moveToFirst()) {
            do {
                int id = c.getInt(c.getColumnIndex("id"));
                String name = c.getString(c.getColumnIndex("name"));
                long timeTotal = c.getLong(c.getColumnIndex("timeTotal"));
                long timeLeft = c.getLong(c.getColumnIndex("timeLeft"));
                long timeStarted = c.getLong(c.getColumnIndex("timeStarted"));
                boolean isRunning;
                if(c.getInt(c.getColumnIndex("isRunning")) == 0) {
                    isRunning = false;
                }
                else {
                    isRunning = true;
                }
                String icon = c.getString(c.getColumnIndex("icon"));
                String sound = c.getString(c.getColumnIndex("sound"));
                TimerObject mTimer = new TimerObject(id, name, timeTotal, timeLeft, timeStarted, isRunning, icon, sound, context.getApplicationContext());
                if(mTimer.isRunning())
                    mTimer.setAlarm();
            } while (c.moveToNext());
        } else
            Log.d("ERROR", "0 rows");
        c.close();
    }
}
