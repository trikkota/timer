package com.d9soft.timer;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Grishanya on 24.01.2016.
 */

public class ListFragment extends Fragment {

    ListAdapter listAdapter;
    static ListView lvMain;
    ArrayList<TimerObject> timers;
    public static final int EDIT_TIMER = 123;
    public static final int ADD_TIMER = 456;
    Timer timer;

    long lastTick;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.list_fragment, container, false);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == ADD_TIMER && resultCode == getActivity().RESULT_OK) {
            String name = data.getStringExtra("name");
            long timeTotal = data.getLongExtra("timeTotal", -1);
            long timeLeft = data.getLongExtra("timeLeft", -1);
            long timeStarted = data.getLongExtra("timeStarted",-1);
            boolean isRunning = data.getBooleanExtra("isRunning", false);
            String icon = data.getStringExtra("icon");
            String sound = data.getStringExtra("sound");
            TimerObject mTimer = new TimerObject(-1, name, timeTotal, timeLeft, timeStarted, isRunning, icon, sound, this.getActivity().getApplicationContext());
            MainActivity.writeToDB(mTimer);
            timers.add(mTimer);
            listAdapter.notifyDataSetChanged();
        }
        else if(requestCode == EDIT_TIMER && resultCode == getActivity().RESULT_OK) {
            int id = data.getIntExtra("id", -1);
            TimerObject mTimer = null;
            for(int i=0;i<timers.size();i++){
                if(timers.get(i).getId() == id)
                    mTimer = timers.get(i);
            }
            if(mTimer != null) {
                mTimer.stop();
                mTimer.setTimeLeft(data.getLongExtra("timeLeft", 0));
                if (data.getBooleanExtra("isRunning", false))
                    mTimer.start();
                mTimer.setTimeTotal(data.getLongExtra("timeTotal", 0));
                mTimer.setName(data.getStringExtra("name"));
                mTimer.setIcon(data.getStringExtra("icon"));
                mTimer.setSound(data.getStringExtra("sound"));
                MainActivity.writeToDB(mTimer);
                listAdapter.notifyDataSetChanged();
            }
        }else if(requestCode == EDIT_TIMER && resultCode == MainActivity.RESULT_DELETE_TIMER) {
            int id = data.getIntExtra("id", -1);
            for(int i = 0; i<timers.size();i++){
                if(timers.get(i).getId() == id){
                    timers.get(i).stop();
                    MainActivity.removeFromDB(id);
                    timers.remove(i);
                    listAdapter.notifyDataSetChanged();
                }
            }
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState){
        super.onViewCreated(view, savedInstanceState);
        timer = new Timer();
        timers = MainActivity.getTimersFromDB(getActivity().getApplicationContext());
        listAdapter = new ListAdapter(getActivity(), timers);
        lvMain = (ListView) getView().findViewById(R.id.lvMain);
        lvMain.setAdapter(listAdapter);
        getView().findViewById(R.id.bAdd).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), EditTimerActivity.class);
                intent.setAction(String.valueOf(ADD_TIMER));
                startActivityForResult(intent, ADD_TIMER);
            }
        });
        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                timers.get(position).switchTimer();
            }
        });
        lvMain.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getActivity(), EditTimerActivity.class);
                intent.setAction(String.valueOf(EDIT_TIMER));
                intent.putExtra("id", timers.get(position).id);
                intent.putExtra("name", timers.get(position).name);
                intent.putExtra("timeTotal", timers.get(position).timeTotal);
                intent.putExtra("timeLeft", timers.get(position).timeLeft);
                intent.putExtra("sound", timers.get(position).sound);
                intent.putExtra("isRunning", timers.get(position).isRunning());
                intent.putExtra("timeStarted", timers.get(position).timeStarted);
                startActivityForResult(intent, EDIT_TIMER);
                return true;
            }
        });
    }

    public void onPause(){
        super.onPause();
        removeFieldUpdater();
    }

    public void onResume() {
        super.onResume();
        setFieldUpdater();
    }

    void setFieldUpdater(){
        removeFieldUpdater();
        timer = new Timer();
        timer.schedule(new updateFields(lvMain), 0, 100);
    }

    void removeFieldUpdater(){
        timer.cancel();
        timer.purge();
    }

    public class updateFields extends TimerTask {

        ListView lv;

        updateFields(ListView _lv){
            lv = _lv;
        }

        @Override
        public void run() {
            if (lvMain.getChildCount()!=0)
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for(int i=0;i<lvMain.getChildCount();i++) {
                            if(timers.get(i+lvMain.getFirstVisiblePosition()).isRunning()) {
                                View v = lvMain.getChildAt(i);
                                TextView tvTimeLeft = (TextView) v.findViewById(R.id.tvTimeLeft);
                                TimerObject iTimer = timers.get(i + lvMain.getFirstVisiblePosition());
                                String timeLeft = MainActivity.millisToString(iTimer.timeLeft-(System.currentTimeMillis() - iTimer.timeStarted));
                                tvTimeLeft.setText(timeLeft);
                            }
                        }
                    }
                });
        }
    }
     public static ListView getListView(){
         return lvMain;
     }
}