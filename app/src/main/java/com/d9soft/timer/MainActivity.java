package com.d9soft.timer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

    private static SQLiteDatabase DB;
    public static final int RESULT_DELETE_TIMER = 3;
    static Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this.getApplicationContext();

        DB = this.openOrCreateDatabase("timers.db", Context.MODE_PRIVATE, null);
        DB.execSQL("CREATE TABLE if not exists Timers (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, icon TEXT, sound TEXT, timeTotal INTEGER, timeLeft INTEGER, timeStarted INTEGER, isRunning INTEGER)");

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Timers"));
        tabLayout.addTab(tabLayout.newTab().setText("Stopwatch"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        final PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public static ArrayList<TimerObject> getTimersFromDB(Context _context){
        ArrayList<TimerObject> timers = new ArrayList<TimerObject>();
        Cursor c = DB.rawQuery("SELECT * FROM Timers",null);
        if (c.moveToFirst()) {
            do {
                int id = c.getInt(c.getColumnIndex("id"));
                String name = c.getString(c.getColumnIndex("name"));
                long timeTotal = c.getLong(c.getColumnIndex("timeTotal"));
                long timeLeft = c.getLong(c.getColumnIndex("timeLeft"));
                long timeStarted = c.getLong(c.getColumnIndex("timeStarted"));
                boolean isRunning;
                if(c.getInt(c.getColumnIndex("isRunning")) == 0) {
                    isRunning = false;
                }
                else {
                    isRunning = true;
                }
                String icon = c.getString(c.getColumnIndex("icon"));
                String sound = c.getString(c.getColumnIndex("sound"));
                TimerObject mTimer = new TimerObject(id, name, timeTotal, timeLeft, timeStarted, isRunning, icon, sound, _context);
                timers.add(mTimer);
            } while (c.moveToNext());
        } else
            Log.d("EROOR", "0 rows");
        c.close();
        return timers;
    }

    public static void writeToDB(TimerObject timer){
        ContentValues values = new ContentValues();
        values.put("timeTotal", timer.timeTotal);
        values.put("timeLeft", timer.timeLeft);
        if(timer.isRunning()) {
            values.put("isRunning", 1);
            values.put("timeStarted", timer.timeStarted);
        }
        else {
            values.put("isRunning", 0);
        }
        values.put("name", timer.name);
        values.put("icon", timer.icon);
        values.put("sound", timer.sound);
        if(timer.getId() != -1) {
            values.put("id", timer.getId());
            DB.update("Timers", values, "id=?", new String[]{String.valueOf(timer.getId())});
        }else{
            int id = (int) DB.insert("Timers", null, values);
            timer.setId(id);
        }
    }

    public static boolean removeFromDB(int id){
        int delCount = DB.delete("Timers", "id = " + id, null);
        if(delCount>0) return true;
        return false;
    }

    public static Bitmap getScaledBitmap(Bitmap bmp){
        float aspectRatio = bmp.getWidth() /
                (float) bmp.getHeight();
        int width = 100;
        int height = Math.round(width / aspectRatio);
        bmp = Bitmap.createScaledBitmap(bmp, width, height, false);
        return bmp;
    }

    public static String millisToString(long millis){
        long mSeconds = (millis / 1000) % 60;
        long mMinutes = (millis / (1000 * 60)) % 60;
        long mHours = (millis / (1000 * 60 * 60)) % 103;
        String time = String.format("%02d:%02d:%02d", mHours, mMinutes, mSeconds);
        return time;
    }

}
