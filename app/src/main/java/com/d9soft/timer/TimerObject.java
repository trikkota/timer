package com.d9soft.timer;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.util.Log;

import java.util.Calendar;

/**
 * Created by Grishanya on 24.01.2016.
 */
public class TimerObject {

    int id;
    Context context;
    String name;
    long timeTotal;
    long timeLeft;
    long timeStarted;
    String icon;
    String sound;
    boolean isRunning;
    AlarmManager alarmManager;

    TimerObject(int _id, String _name, long _timeTotal, long _timeLeft, long _timeStarted, boolean _isRunning, String _icon, String _sound, Context _context){
        id = _id;
        name = _name;
        timeTotal = _timeTotal;
        timeLeft = _timeLeft;
        timeStarted = _timeStarted;
        isRunning = _isRunning;
        icon = _icon;
        sound = _sound;
        context = _context;
        alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
    }

    public int getId(){
        return id;
    }

    public void setContext(Context _context){
        context = _context;
    }

    public void start(){
        if(!isRunning){

            //start AlarmManager
            //record it's id
            timeStarted = System.currentTimeMillis();
            isRunning = true;
            setAlarm();
            MainActivity.writeToDB(this);
        }
    }

    public void stop(){
        if(isRunning) {
            //stop alarm manager
            removeAlarm();
            isRunning = false;
            timeLeft -= (System.currentTimeMillis() - timeStarted);
            MainActivity.writeToDB(this);
        }

    }

    public void switchTimer(){
        if(isRunning)
            stop();
        else
            start();
    }

    public void setAlarm() {
        long when = timeStarted + timeLeft;
        Log.e("now on set", String.valueOf(System.currentTimeMillis()));
        Log.e("when on set", String.valueOf(when));
        Intent intent = new Intent(context, ReminderService.class);
        intent.putExtra("name",name);
        intent.putExtra("sound",sound);
        intent.putExtra("icon",icon);
        PendingIntent pendingIntent = PendingIntent.getService(context, id, intent, 0);
        alarmManager.set(AlarmManager.RTC_WAKEUP, when, pendingIntent);
    }

    public void removeAlarm() {
        Intent intent = new Intent(context, ReminderService.class);
        intent.putExtra("name",name);
        intent.putExtra("sound",sound);
        intent.putExtra("icon",icon);
        PendingIntent pendingIntent = PendingIntent.getService(context, id, intent, 0);
        alarmManager.cancel(pendingIntent);
        Log.e("canceled", "alarm");
    }

    public void tick(long l) {
        timeLeft -= l;
    }

    public boolean isRunning(){
        return isRunning;
    }

    public void setId(int _id){
        id = _id;
    }

    public void setTimeLeft(long _timeLeft) {
        timeLeft = _timeLeft;
    }

    public void setTimeTotal(long _timeTotal) {
        timeTotal = _timeTotal;
    }

    public void setName(String _name) {
        name = _name;
    }

    public void setIcon(String _icon) {
        icon = _icon;
    }

    public void setSound(String _sound) {
        sound = _sound;
    }

}
