package com.d9soft.timer;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.InputStream;
import java.util.Timer;
import java.util.TimerTask;

public class EditTimerActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener {

    long timeStarted = 0,  timeLeft =0;
    long timePassed = 0;
    int hours10=0,hours1=0,minutes10=0,minutes1=0, seconds10=0, seconds1=0;
    TextView tvTimeTotal, tvTimeLeft;
    Intent returnIntent;
    SeekBar sbHours10, sbHours1, sbMinutes10, sbMinutes1, sbSeconds10,sbSeconds1;
    ImageView ivIcon1,ivIcon2,ivIcon3,ivIcon4,ivIcon5, ivIcon6,ivIcon7,ivIconChoose;
    EditText etName;
    String selectedIcon = "0", selectedSound = "0";
    boolean initialized = false;
    View.OnClickListener iconsOnClickListener;

    final static int SELECT_IMAGE = 141;
    final static int SELECT_SOUND = 121;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_timer);
        returnIntent = new Intent();
        etName = (EditText)findViewById(R.id.etName);
        tvTimeTotal = (TextView)findViewById(R.id.ET_tvTimeTotal);
        tvTimeLeft = (TextView)findViewById(R.id.ET_tvTimeLeft);

        sbHours10 = (SeekBar)findViewById(R.id.sbHours10);
        sbHours1 = (SeekBar)findViewById(R.id.sbHours1);
        sbMinutes10 = (SeekBar)findViewById(R.id.sbMinutes10);
        sbMinutes1 = (SeekBar)findViewById(R.id.sbMinutes1);
        sbSeconds10 = (SeekBar)findViewById(R.id.sbSeconds10);
        sbSeconds1 = (SeekBar)findViewById(R.id.sbSeconds1);
        sbHours10.setOnSeekBarChangeListener(this);
        sbHours1.setOnSeekBarChangeListener(this);
        sbMinutes10.setOnSeekBarChangeListener(this);
        sbMinutes1.setOnSeekBarChangeListener(this);
        sbSeconds10.setOnSeekBarChangeListener(this);
        sbSeconds1.setOnSeekBarChangeListener(this);

        iconsOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deselectIcons();
                v.setBackgroundColor((new Color()).argb(150,178,235,242));
                selectedIcon = (String)v.getTag();
            }
        };
        initializeIconViews();

        Button bSound = (Button)findViewById(R.id.bSound);
        bSound.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("audio/mpeg");
                startActivityForResult(Intent.createChooser(intent, "Select audio file"), SELECT_SOUND);
            }
        });

        ivIconChoose = (ImageView)findViewById(R.id.ivIconChoose);
        ivIconChoose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                startActivityForResult(intent, SELECT_IMAGE);
            }
        });

        returnIntent.putExtra("id", -1);
        returnIntent.putExtra("isRunning", false);
        Button bDelete = (Button)findViewById(R.id.bDelete);
        if(getIntent().getAction().equals(String.valueOf(ListFragment.EDIT_TIMER))){
            bDelete.setVisibility(View.VISIBLE);
            returnIntent.putExtra("id", getIntent().getIntExtra("id", -1));
            etName.setText(getIntent().getStringExtra("name"));
            getTimeFromTimer(getIntent().getLongExtra("timeTotal", 0), getIntent().getLongExtra("timeLeft", 0));
            boolean isRunning = getIntent().getBooleanExtra("isRunning",false);
            returnIntent.putExtra("isRunning", isRunning);
            if (isRunning) {
                setFieldUpdater();
                returnIntent.putExtra("timeStarted", getIntent().getLongExtra("timeStarted", 0));
            }
        }
        initialized = true;
        Button btCancel = (Button)findViewById(R.id.btCancel);
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        });
        Button btSave = (Button)findViewById(R.id.btSave);
        btSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if((getTotalMillis()!=0)&&((timeLeft-timePassed)>0)){
                    returnIntent.putExtra("name", ""+etName.getText());
                    returnIntent.putExtra("sound", selectedSound);
                    returnIntent.putExtra("icon", selectedIcon);
                    returnIntent.putExtra("timeTotal", getTotalMillis());
                    returnIntent.putExtra("timeLeft", (timeLeft-timePassed));
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }else{
                    Log.e("Fuck", "No");
                    Toast.makeText(EditTimerActivity.this, "Invalid time settings", Toast.LENGTH_SHORT).show();
                }
            }
        });
        bDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(MainActivity.RESULT_DELETE_TIMER, returnIntent);
                finish();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == SELECT_IMAGE) {
            Uri selectedImageUri = data.getData();
            String s = getRealPathFromURI(selectedImageUri);
            Log.e("Nofunc", selectedImageUri.toString());
            Log.e("Func", s);
            selectedIcon = s;
            deselectIcons();
            try {
                InputStream inputStream = getApplicationContext().getContentResolver().openInputStream(data.getData());
                ivIconChoose.setImageBitmap(MainActivity.getScaledBitmap(BitmapFactory.decodeStream(inputStream)));
                ivIconChoose.setBackgroundColor((new Color()).argb(150,178,235,242));
            }catch(Exception e){
                e.printStackTrace();
            }
        }else if (resultCode == RESULT_OK && requestCode == SELECT_SOUND){
            Uri selectedSoundUri = data.getData();
            String s = getRealPathFromURI(selectedSoundUri);
            selectedSound = s;
            ((TextView)findViewById(R.id.tvSound)).setText(getFileName(selectedSoundUri));
        }
    }

    public String getRealPathFromURI(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor
                .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    void deselectIcons() {
        ivIcon1.setBackgroundColor(Color.TRANSPARENT);
        ivIcon2.setBackgroundColor(Color.TRANSPARENT);
        ivIcon3.setBackgroundColor(Color.TRANSPARENT);
        ivIcon4.setBackgroundColor(Color.TRANSPARENT);
        ivIcon5.setBackgroundColor(Color.TRANSPARENT);
        ivIcon6.setBackgroundColor(Color.TRANSPARENT);
        ivIcon7.setBackgroundColor(Color.TRANSPARENT);
        ivIconChoose.setBackgroundColor(Color.TRANSPARENT);
    }

    public class updateFields extends TimerTask {

        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    timePassed = System.currentTimeMillis() - timeStarted;
                    tvTimeLeft.setText(MainActivity.millisToString(timeLeft - timePassed));
                }
            });
        }
    }

    void setFieldUpdater(){
        timeStarted = getIntent().getLongExtra("timeStarted", 0);
        Timer mTimer = new Timer();
        mTimer.schedule(new updateFields(), 0, 100);
    }

    void getTimeFromTimer(long timeTotal, long _timeLeft){
        long totalSeconds = (timeTotal / 1000) % 60;
        long totalMinutes = (timeTotal / (1000 * 60)) % 60;
        long totalHours = (timeTotal / (1000 * 60 * 60)) % 103;

        timeLeft =  _timeLeft;

        sbHours10.setProgress((int)(totalHours/10));
        sbHours1.setProgress((int)(totalHours - ((int)(totalHours/10))*10));
        sbMinutes10.setProgress((int)(totalMinutes/10));
        sbMinutes1.setProgress((int)(totalMinutes - ((int)(totalMinutes/10))*10));
        sbSeconds10.setProgress((int)(totalSeconds/10));
        sbSeconds1.setProgress((int) (totalSeconds - ((int) (totalSeconds / 10)) * 10));

        //tvTimeLeft.setText(millisToString(timeLeft));

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()){
            case R.id.sbHours10:
                if(initialized)
                    timeLeft += 1000*(progress - hours10)*10*60*60;
                hours10 = progress;
                break;
            case R.id.sbHours1:
                if(initialized)
                    timeLeft += 1000 * (progress - hours1) * 60 * 60;
                hours1 = progress;
                break;
            case R.id.sbMinutes10:
                if(initialized)
                    timeLeft += 1000*(progress - minutes10)*10*60;
                minutes10 = progress;
                break;
            case R.id.sbMinutes1:
                if(initialized)
                    timeLeft += 1000*(progress - minutes1)*60;
                minutes1 = progress;
                break;
            case R.id.sbSeconds10:
                if(initialized)
                    timeLeft += 1000*(progress - seconds10)*10;
                seconds10 = progress;
                break;
            case R.id.sbSeconds1:
                if(initialized)
                    timeLeft += 1000*(progress - seconds1);
                seconds1 = progress;
                break;
        }
        displayTime();
    }

    void displayTime(){
        tvTimeTotal.setText(MainActivity.millisToString(getTotalMillis()));
        tvTimeLeft.setText(MainActivity.millisToString(timeLeft - timePassed));
    }

    long getTotalMillis(){
        return 1000*(seconds1+seconds10*10+(minutes1+minutes10*10)*60+(hours1+hours10*10)*60*60);
    }

    void initializeIconViews(){
        ivIcon1 = (ImageView)findViewById(R.id.ivIcon1);
        ivIcon2 = (ImageView)findViewById(R.id.ivIcon2);
        ivIcon3 = (ImageView)findViewById(R.id.ivIcon3);
        ivIcon4 = (ImageView)findViewById(R.id.ivIcon4);
        ivIcon5 = (ImageView)findViewById(R.id.ivIcon5);
        ivIcon6 = (ImageView)findViewById(R.id.ivIcon6);
        ivIcon7 = (ImageView)findViewById(R.id.ivIcon7);
        ivIcon1.setOnClickListener(iconsOnClickListener);
        ivIcon2.setOnClickListener(iconsOnClickListener);
        ivIcon3.setOnClickListener(iconsOnClickListener);
        ivIcon4.setOnClickListener(iconsOnClickListener);
        ivIcon5.setOnClickListener(iconsOnClickListener);
        ivIcon6.setOnClickListener(iconsOnClickListener);
        ivIcon7.setOnClickListener(iconsOnClickListener);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }
}
