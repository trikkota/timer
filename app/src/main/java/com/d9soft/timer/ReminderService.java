package com.d9soft.timer;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.io.File;

/**
 * Created by Grishanya on 29.01.2016.
 */
public class ReminderService extends IntentService {

    private static final int NOTIF_ID = 1;

    public ReminderService(){
        super("ReminderService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.e("SERVICE", "Gets intent");
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        long when = System.currentTimeMillis();         // notification time
        Notification notification = new Notification(R.mipmap.ic_launcher, "Timer", when);
        notification.sound = Uri.fromFile(new File(intent.getStringExtra("sound")));
        notification.defaults = Notification.DEFAULT_LIGHTS | Notification.DEFAULT_VIBRATE;
        //notification.defaults |= Notification.DEFAULT_SOUND;
        notification.flags |= notification.FLAG_INSISTENT;
        Intent notificationIntent = new Intent(this, MainActivity.class);
        //could pass timer info to activity in notificationIntent
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent , 0);
        notification.setLatestEventInfo(getApplicationContext(), "Time came. " + intent.getStringExtra("name"), "You should open the app now", contentIntent);
        nm.notify(NOTIF_ID, notification);
    }

}